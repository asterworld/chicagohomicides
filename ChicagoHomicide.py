#!/usr/bin/env python
# coding: utf-8

# # __Zločinnost v Chicagu - kde se nechat zavraždit  - Mikulík__
# 

# # Budeme se snažit zjistit, kde se nejvíc vraždí v Chicagu a tedy zjistit, kde radši nebýt :-)

# Začneme naimportováním všech nutných knihoven a csv/xlsc souborů.

# Instalace potřebných importů:
#     pip install pandas
#     pip install folium
#     pip install seaborn
#     pip install plotly
#     pip install matplotlib

# In[22]:


import datetime
import seaborn as sns
import pandas as pd
import folium
import plotly.express as px
import matplotlib.pyplot as plt
from datetime import datetime

df1 = pd.read_csv('2019.csv')
df2 = pd.read_csv('2020.csv')
df3 = pd.read_csv('2021.csv')
df4 = pd.read_csv('2022.csv')

pop = pd.read_excel('chicagoPop.xlsx')


# Spojíme dataframy roků do jednoho:

# In[23]:


df2 = pd.concat([df2, df3], axis=0)
df1 = pd.concat([df1, df2], axis=0)
df = pd.concat([df1, df4], axis=0)
#odstaníme nepotřebné sloupce
df = df.drop(columns=['Case Number','IUCR','Description','Domestic', 'Beat', "District", "Ward", "FBI Code", "X Coordinate", "Y Coordinate", "Updated On",'Location'])
df = df.dropna()


# Prozkoumáme sjednocený dataframe zločinů za roky 2018 - 2022 v Chicagu:

# In[24]:


df.head(5)


# Prozkoumáme dataframe populace jednodlivých oblastí:

# In[25]:


pop.head(5)


# Upravíme si data pro populaci a označíme každou komunitní oblast číslem od jedničky

# In[26]:


pop = pop.reset_index()
pop.rename(columns={'index':'Community Area'}, inplace=True)
pop['Community Area'] = pop['Community Area'] + 1


# Upravíme si data zločinů za jednotlivé roky extrahováním časových údajů ze sloupce Date do samostatných sloupců (Hour a Month)

# In[27]:


# extrahování hodin ze sloupce Date a vytvoření nového Hour
df['Hour'] = df['Date'].apply(lambda x: datetime.strptime(x, '%m/%d/%Y %I:%M:%S %p').hour)
# extrahování měsíce ze sloupce Date a vytvoření nového Month
df['Month'] = df['Date'].apply(lambda x: datetime.strptime(x, '%m/%d/%Y %I:%M:%S %p').month)


# Nyní spojíme dataframe zločinů s dataframem populace jednotlivých oblastí, abychom s tím mohli dále pracovat

# In[28]:


df = pd.merge(df, pop, on='Community Area', how='inner')


# In[29]:


df.head(5)


# # Zjistíme si průměrné počty zločinů a vražd ať máme přehled do čeho jdeme

# Výpočet průměrného počtu zločinů za rok:

# In[30]:


homicideAll = df.groupby(["Year"])["Primary Type"].count().mean()
print(homicideAll)


# Z toho počet průměrně vražd ročně:

# In[31]:


homicideMean = df[df["Primary Type"] == "HOMICIDE"].groupby(["Year"])["Primary Type"].count().mean()
print(homicideMean)


# Výpočet průměrného počtu zločinů za měsíc:

# In[32]:


df.groupby(["Year","Month"])["Primary Type"].count().mean()


# Z toho průměrně vražd měsíčně:

# In[33]:


df[df["Primary Type"] == "HOMICIDE"].groupby(["Year", "Month"])["Primary Type"].count().mean()


# A teď si vypočítáme procentuální průměrný podíl vražd na všech trestných činech:

# In[100]:


(homicideMean) / (homicideAll) * 100


# Tj. procentuálně dělá podíl vražd na celku okolo 0,3%

# # Nyní můžeme začít s vizualizací:

# Prvně si znázorníme počty trestních činů dle typu:

# In[35]:


plt.figure(figsize=(15,5))
ax = sns.countplot(x = df['Primary Type'], orient='v', order = df['Primary Type'].value_counts().index)
ax.set_xticklabels(ax.get_xticklabels(),rotation = 90)
ax.set_ylabel('Number of crimes')
ax.set_xlabel('Crime Type')


# Znázornění intezity trestných činů v závislosti na denní hodině pomocí Kernel density estimation (KDE):

# In[106]:


ct = df[df['Primary Type'].isin(['THEFT','BATTERY','ROBBERY','CRIMINAL DAMAGE','NARCOTICS','ASSAULT','HOMICIDE'])]
plt.figure(figsize=(16,10))
ax = sns.displot(x = 'Hour', hue="Primary Type", data = ct, kind="kde", height=9, aspect=1.5)
plt.xlim(0, 23)


# Znázornění intezity vražd a únosů v závislosti na denní hodině pomoci Kernel density estimation (KDE):

# In[108]:


ct = df[df['Primary Type'].isin(['HOMICIDE','KIDNAPPING'])]
plt.figure(figsize=(16,14))
ax = sns.displot(x = 'Hour', hue="Primary Type", data = ct, kind="kde", height=9, aspect=1.5)
plt.xlim(0, 23)


# Grafy kriminality v jednotlivých oblastech:

# In[38]:


df_area = df['Area'].value_counts().reset_index().rename(columns={'index':'Area','Area':'Number of crimes'})
fig = px.bar(df_area, x='Area', y='Number of crimes', title='Count of crimes in each area')
fig.update_layout(xaxis_title = 'Area of Chicago', yaxis_title = 'Number of crimes')
fig.show()


# In[71]:


df_area = df['Name'].value_counts().reset_index().rename(columns={'index':'Name','Name':'Number of crimes'})
fig = px.bar(df_area, x='Name', y='Number of crimes', title='Count of crimes in each name')
fig.update_layout(xaxis_title = 'Community area of Chicago', yaxis_title = 'Number of crimes')
fig.show()


# Vypíšeme si tabulku dle vražd na 100k obyvatel v jednotlivých oblastech:

# In[78]:


df_homicideOnly = df[df['Primary Type'] == 'HOMICIDE']
f = df_homicideOnly.groupby('Name').count()
areaname = f[['ID']]
areaname = areaname.reset_index()
areaname = areaname.rename(columns = {'ID':'Homicide Records'})
neighbourhoods = pd.merge(pop, areaname, on='Name', how='inner')
neighbourhoods['Homicide Rate'] = neighbourhoods['Homicide Records'].div(neighbourhoods['Population'])
neighbourhoods['Homicides per 100.000 People'] = neighbourhoods['Homicide Records'].div(neighbourhoods['Population']) * 100000


# In[79]:


neighbourhoods.sort_values('Homicide Rate',ascending = False).head(5)


# Nejvíce vražd na obyvatele je tedy v komunitní oblasti West Garfield Park.

# In[80]:


neighbourhoods.sort_values('Homicide Rate',ascending = True).head(5)


# Nejméně vražd na obyvatele je pak v komunitní oblasti Norwood Park. Té se tedy obloukem vyhneme, protože je v rozporu s cílem naší semestrální práce.

# A nyní vezmeme vraždy víc vizuálně:

# In[43]:


df_homicideOnly = df[df['Primary Type'] == 'HOMICIDE']
df_area = df_homicideOnly['Area'].value_counts().reset_index().rename(columns={'index':'Area','Area':'Number of homicides'})

fig = px.bar(df_area, x='Area', y='Number of homicides', title='Count of homicides in each area')
fig.update_layout(xaxis_title = 'Area of Chicago', yaxis_title = 'Number of homicides')
fig.show()


# In[81]:


df_homicideOnly = df[df['Primary Type'] == 'HOMICIDE']
df_area = df_homicideOnly['Name'].value_counts().reset_index().rename(columns={'index':'Name','Name':'Number of homicides'})

fig = px.bar(df_area, x='Name', y='Number of homicides', title='Count of homicides in each community area')
fig.update_layout(xaxis_title = 'Community area of Chicago', yaxis_title = 'Number of homicides')
fig.show()


# A jeden z nejdůležitejších grafů hledání našeho cíle je počet vražd na 100k obyvatel:

# In[45]:


df_homicideOnly = df[df['Primary Type'] == 'HOMICIDE']
df_homicideOnly = df_homicideOnly['Name'].value_counts().reset_index().rename(columns={'index':'Name','Name':'Number of homicides'})
df_merged = df_homicideOnly.merge(pop, on='Name', how='left')
df_merged['Homicide100k'] = df_merged['Number of homicides'] / df_merged['Population'] * 100000
df_merged = df_merged.sort_values('Homicide100k', ascending=False)
fig = px.bar(df_merged, x='Name', y='Homicide100k', title='Count of homicides per 100,000 population.')
fig.update_layout(xaxis_title = 'Community area of Chicago', yaxis_title = 'Homicides per 100,000 population.')
fig.show()


# První mapa nám znázorňuje veškeré trestné činy v Chicagu, vzorek je omezen, aby výkon byl plynulý:

# In[83]:


df_filtered = df.sample(frac=0.01)
fig = px.scatter_mapbox(df_filtered, lat="Latitude", lon="Longitude", hover_name="Primary Type", color='Primary Type', hover_data=["Location Description", "Area" ,"Name","Arrest"], zoom=10, height=800)
fig.update_layout(mapbox_style="open-street-map")
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
fig.show()


# Nyní si znázorníme a ukážeme mapu jednotlivých komunitních oblastí, aby si každý mohl udělat představu, kde co je. Každá tečka je nějaký zločin, barva je dle komunitní oblasti.

# In[82]:


df_fil = df.sample(frac=0.05)
df_fil['Color'] = df_fil['Name']
fig = px.scatter_mapbox(df_fil, lat="Latitude", lon="Longitude", hover_name="Name",color='Color', hover_data=["Location Description", "Area", "Primary Type", "Arrest"],zoom=10, height=800)
fig.update_layout(mapbox_style="open-street-map")
fig.update_layout(margin={"r": 0, "t": 0, "l": 0, "b": 0})
fig.show()


# Teď už si můžeme znázornit počet vražd dle nádherné heat mapy:

# In[46]:


from folium.plugins import HeatMap
df_homicideOnly = df[df['Primary Type'] == 'HOMICIDE']
m = folium.Map([38.881,-84.623], zoom_start=14)
latlon_2022 = df_homicideOnly[['Latitude','Longitude']]
crime_heatmap = folium.Map(location= [41.881,-87.623], tiles = "CartoDB Positron", zoom_start = 11)
HeatMap(latlon_2022, min_opacity=0.05).add_to(crime_heatmap)
crime_heatmap


# Můžeme jasně vidět, že to koresponduje s daty z předešlých tabulek a vizualizací - West Garfield Park je opravdu "vražedné" a s tím samozřejmě celá West Side.

# Teď už si vizualizujeme mapu Chicaga pouze s vraždami bez znázornění pomocí heat mapy:

# In[49]:


df_homicideOnly = df[df['Primary Type'] == 'HOMICIDE']
df_filtered = df_homicideOnly.sample(frac=0.5)
fig = px.scatter_mapbox(df_filtered, lat="Latitude", lon="Longitude", hover_name="Primary Type", color='Primary Type', hover_data=["Location Description", "Area" ,"Name","Arrest"], zoom=10, height=800)
fig.update_layout(mapbox_style="open-street-map")
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
fig.show()


# Na to navážeme a zjistime denní hodiny největší kriminality:

# In[50]:


crimes_per_hour = df.groupby("Hour")["Primary Type"].count()
ax = crimes_per_hour.plot(kind='line')
ax.set_xticks(range(24))
plt.xlabel("Hour")
plt.ylabel("Number of crimes")
plt.title("Total number of crimes per hour")
plt.show()


# a denní hodinu největší vraždící nálady:

# In[51]:


df_homicideOnly = df[df['Primary Type'] == 'HOMICIDE']
crimes_per_hour = df_homicideOnly.groupby("Hour")["Primary Type"].count()
ax = crimes_per_hour.plot(kind='line')
ax.set_xticks(range(24))
plt.xlabel("Hour")
plt.ylabel("Number of homicides")
plt.title("Total number of homicides per hour")
plt.show()


# Totéž se pokusíme zjistit s kriminalitou v rámci měsíců:

# In[52]:


crimes_per_hour = df.groupby("Month")["Primary Type"].count()

# zobrazení celkového počtu zločinů podle hodiny
ax = crimes_per_hour.plot(kind='line')

ax.set_xticks(range(1,13))
plt.xlabel("Month")
plt.ylabel("Number of crimes")
plt.title("Total number of crimes per month")
plt.show()


# a u vražd:

# In[84]:


df_homicideOnly = df[df['Primary Type'] == 'HOMICIDE']
homicides_per_hour = df_homicideOnly.groupby("Month")["Primary Type"].count()
ax = homicides_per_hour.plot(kind='line')
ax.set_xticks(range(1,13))
plt.xlabel("Month")
plt.ylabel("Number of homicides")
plt.title("Total number of homicides per month")
plt.show()


# Teď zjistíme v jaké lokalitě je největší kriminalita okolo půlnoci, což je denní hodina s největší absolutní kriminalitou dle předchozích grafů:

# In[86]:


df_tw = df[df['Hour'] == 0]
df_f = df_tw['Location Description'].value_counts().reset_index().rename(columns={'index':'Location Description','Location Description':'Number of crimes'}).head(10)
fig = px.bar(df_f, x='Location Description', y='Number of crimes', title='Number of crimes in each location around 00:00' )
fig.update_layout(xaxis_title = 'Location', yaxis_title = 'Number of crimes')
fig.show()


# a to stejné s vraždami:

# In[91]:


df_homicideOnly = df[df['Primary Type'] == 'HOMICIDE']
df_tw = df_homicideOnly[df_homicideOnly['Hour'] == 0]
df_f = df_tw['Location Description'].value_counts().reset_index().rename(columns={'index':'Location Description','Location Description':'Number of homicides'}).head(10)
fig = px.bar(df_f, x='Location Description', y='Number of homicides', title='Number of homicides in each location around 00:00' )
fig.update_layout(xaxis_title = 'Location', yaxis_title = 'Number of homicides')
fig.show()


# k tomu ještě přidáme měsíc s největší kriminalitou a to červenec:

# In[89]:


df_tw = df[(df['Hour'] == 0) & (df['Month'] == 7)]
df_f = df_tw['Location Description'].value_counts().reset_index().rename(columns={'index':'Location Description','Location Description':'Number of crimes'}).head(10)
fig = px.bar(df_f, x='Location Description', y='Number of crimes', title='Number of crimes in each location around 00:00 in July' )
fig.update_layout(xaxis_title = 'Location', yaxis_title = 'Number of crimes')
fig.show()


# a obdobně s vraždami:

# In[90]:


df_homicideOnly = df[df['Primary Type'] == 'HOMICIDE']
df_tw = df_homicideOnly[(df_homicideOnly['Hour'] == 0) & (df_homicideOnly['Month'] == 7)]
df_f = df_tw['Location Description'].value_counts().reset_index().rename(columns={'index':'Location Description','Location Description':'Number of homicides'}).head(10)
fig = px.bar(df_f, x='Location Description', y='Number of homicides', title='Number of homicides in each location around 00:00 in July' )
fig.update_layout(xaxis_title = 'Location', yaxis_title = 'Number of homicides')
fig.show()


# Ulice okolo půlnoci v červenci je tedy bezkonkurenčně nejvražednější.

# # Prvotní závěr našeho hledání:

# Pokud chci být zavražděn, tak mám největší šanci v komunitní oblasti West Garfield Park, v červenci přesně o půlnoci 
# v jakékoliv otevřené ulici. 
# Pokud se nemůžu dostavit konkrétně do komunitní oblasti West Garfield Park, tak se stačí dostavit aspoň do jakékoliv jiné 
# komunitní oblasti ve West Side Chicago a ač bude šance na to být zavražděn menší, tak i tak bude solidní.

# Ještě zkusíme clustery:

# In[58]:


#Tabulka pivotů s indexem 'Area' a sloupci 'Primary Type'
df_homicideOnly = df[df['Primary Type'] == 'HOMICIDE']
table2 = df_homicideOnly.pivot_table(index='Area', columns='Primary Type', values='ID', aggfunc='count')
# Vyplnit všechny chybějící hodnoty nulou
table2.fillna(0, inplace=True)   #cluster s Primary Type a Area


# Použijeme elbow metodu k určení počtu clusteru v datasetu.

# In[96]:


#elbow metoda
## In cluster analysis, the elbow method is a heuristic used in determining the number of clusters in a data set.
from sklearn import preprocessing
df_standardized = preprocessing.scale(table2)
df_standardized = pd.DataFrame(df_standardized)
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')
plt.figure(figsize=(9,7))
wcss = []
for i in range (1, 9):
    kmeans = KMeans(n_clusters = i, init = 'k-means++', n_init='auto', random_state = 42)
    kmeans.fit(df_standardized)
    wcss.append(kmeans.inertia_)
plt.plot(range(1,9),wcss, 'bx-')
plt.title('Elbow Method')
plt.xlabel('Number of clusters')
plt.ylabel('WCSS')
plt.show()


# In[97]:


#Shlukování metodou nejbližších středů
kmeans = KMeans(n_clusters = 3, init = 'k-means++', n_init='auto',random_state = 42)  #clustering 3
y_kmeans = kmeans.fit_predict(df_standardized)
#začátek číslování clusteru od 1 místo 0
y_kmeans1 = y_kmeans + 1
cluster = list(y_kmeans1)
#přidání clusteru do datasetu
table2['cluster'] = cluster
#průměr clusteru
kmeans_mean_cluster = pd.DataFrame(round(table2.groupby('cluster').mean(),1))
kmeans_mean_cluster.plot(kind='bar',stacked = True)
plt.rcParams["figure.figsize"] = (18,13)
plt.title("Cluster analysis")
plt.xlabel("Clusters")
plt.ylabel("Number of homicides")


# Teď zjistíme do jakého clusteru patří co:

# In[98]:


table2[table2['cluster']==1]


# In[62]:


table2[table2['cluster']==2]


# In[63]:


table2[table2['cluster']==3]


# A teď už si připravíme pouze závěrečnou vizualizaci clusterů na mapě:

# In[64]:


c = table2.copy()
c = c.reset_index(drop=False)
c = c[['Area','cluster']]
df_clusters = df_homicideOnly.copy()
df_clusters = pd.merge(df_clusters, c, on='Area', how='inner')


# In[65]:


df_filtered = df_clusters.sample(frac=0.8)
fig = px.scatter_mapbox(df_filtered, lat="Latitude", lon="Longitude", hover_name="Primary Type", color='cluster',color_continuous_scale= ['#009900', '#0000cc', '#cc0000'], hover_data=["Location Description", "Area" ,"Name","Arrest"], zoom=10, height=800)
fig.update_layout(mapbox_style="open-street-map")
fig.update_layout(margin={"r":0,"t":0,"l":0,"b":0})
fig.show()


# # Závěr

# Co se týče závěru z clusterů, tak výsledek vyšel podobný a to nejlepší místo na to být zavražděn je West Side Chicago, což je nadoblast West Garfield Parku, který nám v prvotním závěru vyšel jako nejlepší místo v rámci komunitních oblastí.
# 
# TLDR:
# Pokud chci být opravdu zavražděn, komunitní oblast West Garfield Park ve West Side Chicagu, v červenci přesně o půlnoci 
# v jakékoliv otevřené ulici je to pravé ořechové.
