# Zločinnost v Chicagu - kde se nechat zavraždit

Kód provádí analýzu trestné činnosti v Chicagu a na základě analýzy hledá místo, čas a lokaci, kde dochází nejčastěji k vraždám. Dle toho může doporučit, kde má daná osoba největší šanci být zavražděna.
